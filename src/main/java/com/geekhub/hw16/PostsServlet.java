package com.geekhub.hw16;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Posts", urlPatterns = "/posts")
public class PostsServlet extends HttpServlet {

    private final static int POSTS_ON_PAGE = 7;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      int pageNumber = 1;
      if(request.getParameter("page") != null) {
        pageNumber = Integer.parseInt(request.getParameter("page"));
      }
        try {
            DAO.getConnection();
            request.setAttribute("posts", DAO.getPosts((pageNumber -1)*POSTS_ON_PAGE, POSTS_ON_PAGE));
            int postsCount = DAO.getTotalRows();
            int pagesCount = (int) Math.ceil(postsCount * 1.0 / POSTS_ON_PAGE);
            request.setAttribute("pagesCount", pagesCount);
            request.setAttribute("currentPage", pageNumber);
        } catch (Exception e) {
        }
        request.getRequestDispatcher("/WEB-INF/posts.jsp").forward(request, response);
    }
}
