package com.geekhub.hw16;

import java.sql.Date;

public class Post {

    Date date;
    String name;
    String text;
    int mark;

    public Post(Date date, String name, int mark) {
        this.date = date;
        this.name = name;
        this.mark = mark;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
