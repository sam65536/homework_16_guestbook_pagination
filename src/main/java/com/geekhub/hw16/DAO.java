package com.geekhub.hw16;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DAO {

    public static Connection getConnection() throws Exception {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection("jdbc:postgresql://localhost:5432/test", "root", "");
    }

    public static List<Post> getPosts(int pageNumber, int recordsOnPage) throws Exception {
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement(
                     "SELECT date, name, mark " +
                            "FROM posts ORDER BY DATE DESC " +
                            "LIMIT " + recordsOnPage + " OFFSET " + pageNumber);
             ResultSet resultSet = ps.executeQuery();
        ) {
            ArrayList<Post> posts = new ArrayList<>();
            while (resultSet.next()) {
                Date date = resultSet.getDate(1);
                String name = resultSet.getString(2);
                int mark = resultSet.getInt(3);
                posts.add(new Post(date, name, mark));
            }
            return posts;
        }
    }

    public static void addPost(String name, String text, int mark) throws Exception {
        try (Connection c = getConnection();
             PreparedStatement ps = c.prepareStatement("INSERT INTO posts (date, name, text, mark) VALUES(?,?,?,?)");
        ) {
            Date date = Date.valueOf(LocalDate.now());
            ps.setDate(1, date);
            ps.setString(2, name);
            ps.setString(3, text);
            ps.setInt(4, mark);
            ps.executeUpdate();
        }
    }

    public static int getTotalRows() throws Exception {
        int totalRows = 0;
        try (Connection c = getConnection();
             Statement stmt = c.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT COUNT(id) FROM posts");
        ) {
            if (rs.next()) {
                totalRows= rs.getInt(1);
            }
        }
        return totalRows;
    }
}
